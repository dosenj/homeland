<?php

class ValidateFormElement
{
	private $name;
	private $email;
	private $password;

	public function __construct($name = '', $email = '', $password = '')
	{
		$this->name = $name;
		$this->email = $email;
		$this->password = $password;
	}

	public function validateName()
	{
		$nameError = false;

		if( empty($this->name) ){
			$nameError = 'Field Name can not be empty.';
		} else {

			if( strlen($this->name) < 3 || strlen($this->name) > 15 ){
				$nameError = 'At least 3 characters, but not more than 15';
			}

		}

		return $nameError;
	}

	public function validateEmail()
	{
		$emailError = false;

		if( empty($this->email) ){
			$emailError = 'Field Email can not be empty.';
		} else {

			if( !filter_var($this->email, FILTER_VALIDATE_EMAIL) ){
				$emailError = 'Email is not valid.';
			}

		}

		return $emailError;

	}

	public function validatePassword()
	{
		$passwordError = false;

		if( empty($this->password) ){
			$passwordError = 'Field Password can not be empty.';
		} else {

			if( strlen($this->password) < 3 || strlen($this->password) > 15 ){
				$passwordError = 'At least 3 characters, but not more than 15.';
			}

		}

		return $passwordError;

	}
}