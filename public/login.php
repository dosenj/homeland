<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Login</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="icon" type="image/png" href="images/favicon.png">
		<link rel="stylesheet" type="text/css" href="css/app.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	</head>
	<body>

		<div id="navigation">
			<ul>
				<li><a href="register.php">REGISTER</a></li>
				<li><a href="login.php">LOGIN</a></li>
			</ul>
		</div>

		<div id="login-content">
			<form method="post" action="login.php" id="user-login-form">
				<div id="form-elements-container">
					<div id="email-container">
						<label for="email">Email:</label>
						<input type="text" name="email" id="email" autocomplete="off" />
					</div>
					<div id="password-container">
						<label for="password">Password:</label>
						<input type="password" name="password" id="password" />
					</div>
					<div id="submit-container">
						<button id="login" type="button">LOGIN</button>
					</div>
				</div>
				<input type="hidden" name="user-log" value="user-log-ok" />
			</form>
		</div>

		<div id="image-container">
			
		</div>

		<script type="text/javascript" src="js/app.js"></script>
	</body>
</html>