<?php

	if( isset($_POST['user-reg']) && $_POST['user-reg'] == 'user-reg-ok' ){

		require __DIR__ . '/../include/ValidateFormElement.php';

		$name = $_POST['name'];
		$email = $_POST['email'];
		$password = $_POST['password'];

		$validate = new ValidateFormElement($name, $email, $password);

		$validateName = $validate->validateName();

		$validateEmail = $validate->validateEmail();

		$validatePassword = $validate->validatePassword();

		if( $validateName === false ){
			$nameOk = $name;
		}

		if( $validateEmail === false ){
			$emailOk = $email;
		}

		if( $validateName === false && $validateEmail == false && $validatePassword === false ){

			echo 'Well and Good.';

			// data valid and ready for insert

		}		

	}

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Register</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="icon" type="image/png" href="images/favicon.png">
		<link rel="stylesheet" type="text/css" href="css/app.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	</head>
	<body>

		<div id="navigation">
			<ul>
				<li><a href="register.php">REGISTER</a></li>
				<li><a href="login.php">LOGIN</a></li>
			</ul>
		</div>

		<div id="register-content">
			<form method="post" action="register.php" id="user-register-form">
				<div id="form-elements-container">
					<div id="name-container">
						<label for="name">Name:</label>
						<input type="text" name="name" id="name" autocomplete="off" minlength="3" maxlength="15" class="<?php echo ( isset($validateName) && !empty($validateName) ) ? 'error' : ''; ?>" value="<?php echo ( isset($nameOk) && !empty($nameOk) ) ? $nameOk : ''; ?>" />
						<div id="name-error-message"><?php echo ( isset($validateName) && !empty($validateName) ) ? $validateName : ''; ?></div>
					</div>
					<div id="email-container">
						<label for="email">Email:</label>
						<input type="text" name="email" id="email" autocomplete="off" class="<?php echo ( isset($validateEmail) && !empty($validateEmail) ) ? 'error' : ''; ?>" value="<?php echo ( isset($emailOk) && !empty($emailOk) ) ? $emailOk : ''; ?>" />
						<div id="email-error-message"><?php echo ( isset($validateEmail) && !empty($validateEmail) ) ? $validateEmail : ''; ?></div>
					</div>
					<div id="password-container">
						<label for="password">Password:</label>
						<input type="password" name="password" id="password" minlength="3" maxlength="15" class="<?php echo ( isset($validatePassword) && !empty($validatePassword) ) ? 'error' : ''; ?>" />
						<div id="password-error-message"><?php echo ( isset($validatePassword) && !empty($validatePassword) ) ? $validatePassword : ''; ?></div>
					</div>
					<div id="submit-container">
						<button id="register" type="button">REGISTER</button>
					</div>
				</div>
				<input type="hidden" name="user-reg" value="user-reg-ok" />
			</form>
		</div>

		<div id="image-container">
			
		</div>

		<div id="confirm-register-container" style="display: none;">
			<div id="confirm-register" style="display: none;">
				<h3>Info</h3>
				<p>Are you sure you want to register to our site ?</p>
				<div>
					<button id="register-confirm-yes">Yes</button>
				</div>
				<div>
					<button id="register-confirm-no">No</button>
				</div>
			</div>
		</div>

		<script type="text/javascript" src="js/app.js"></script>
	</body>
</html>