$(document).ready(function(){

	$("button#register").click(function(){
		showConfirmDialog();
	});

	$("button#register-confirm-yes").click(function(){
		closeConfirmDialog();
		validateUserRegistration();
	});

	$("button#register-confirm-no").click(function(){
		closeConfirmDialog();
	});

});

function showConfirmDialog()
{
	$("#confirm-register").css({"display":"block"});
	$("#confirm-register-container").css({"display":"block"});
	$("body").css({"overflow":"hidden"});
}

function closeConfirmDialog()
{
	$("#confirm-register").css({"display":"none"});
	$("#confirm-register-container").css({"display":"none"});
	$("body").css({"overflow":""});
}

function validateUserRegistration()
{
	var name = $("#name").val();
	var email = $("#email").val();
	var password = $("#password").val();

	var errors = [];
	var error = false;

	var nameError = '';
	var emailError = '';
	var passwordError = '';

	if( name == '' ){
		nameError = 'Field Name can not be empty.';
		errors.push(nameError);
		error = true;
		$("#name").css({"border":"1px solid red"});
		$("#name-error-message").text(nameError);
	} else{

		var nameLength = name.length;
		
		if( nameLength < 3 || nameLength > 15 ){
			nameError = 'At least 3 characters, but not more than 15';
			errors.push(nameError);
			error = true;
			$("#name").css({"border":"1px solid red"});
			$("#name-error-message").text(nameError);
		}

	}

	if( nameError == '' ){
		$("#name").css({"border":""});
		$("#name-error-message").text('');
	}

	if( email == '' ){
		emailError = 'Field Email can not be empty.';
		errors.push(emailError);
		error = true;
		$("#email").css({"border":"1px solid red"});
		$("#email-error-message").text(emailError);
	} else {

		var regularExpressionForEmail = /\S+@\S+\.\S+/;

		var emailPass = regularExpressionForEmail.test(email);

		if( emailPass === false ){
			emailError = 'Email is not valid.';
			errors.push(emailError);
			error = true;
			$("#email").css({"border":"1px solid red"});
			$("#email-error-message").text(emailError);
		}

	}

	if( emailError == '' ){
		$("#email").css({"border":""});
		$("#email-error-message").text('');
	}

	if( password == '' ){
		passwordError = 'Field Password can not be empty.';
		errors.push(passwordError);
		error = true;
		$("#password").css({"border":"1px solid red"});
		$("#password-error-message").text(passwordError);
	} else {

		var passwordLength = password.length;

		if( passwordLength < 3 || passwordLength > 15 ){
			passwordError = 'At least 3 characters, but not more than 15.';
			errors.push(passwordError);
			error = true;
			$("#password").css({"border":"1px solid red"});
			$("#password-error-message").text(passwordError);
		}

	}

	if( passwordError == '' ){
		$("#password").css({"border":""});
		$("#password-error-message").text('');
	}

	if( error === false ){
		$("#user-register-form").submit();
	}

}